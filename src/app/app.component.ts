import { Component } from '@angular/core';

interface Animal {
  id: number;
  nombre: string;
}
@Component({
  selector: 'pruebas-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private urls = {
    perretes: 'http://patasbox.es/wp-content/uploads/2016/09/pug-rain-coat.jpg',
    gatetes: 'https://pbs.twimg.com/media/D12gXVyWkAAQ8rL.jpg'
  };
  public tipoAnimal = 'gatetes';
  public urlImagen = this.urls[this.tipoAnimal];
  public fotoBonita = false;
  public radioEsquinas = 0;
  public animales: Animal[] = [
    {
      id: 1,
      nombre: 'Toby'
    },
    {
      id: 2,
      nombre: 'Mario'
    },
    {
      id: 3,
      nombre: 'Sira'
    }
  ];
  public nombreNuevo = '';

  constructor() {
    setTimeout(() => {
      this.tipoAnimal = 'gatetes';
    }, 10000);
  }

  public anyadirAnimal() {
    this.animales.push({
      id: this.animales[this.animales.length - 1].id + 1,
      nombre: this.nombreNuevo
    });
    this.nombreNuevo = '';
  }

  public borrarAnimal(event: Event, animal: Animal) {
    const i = this.animales.indexOf(animal);
    this.animales.splice(i, 1);
  }

  public esGatete() {
    return this.tipoAnimal === 'gatetes';
  }

  private cambiaAnimal(e: Event) {
    e.preventDefault();
    this.tipoAnimal = 'perretes';
    this.urlImagen = this.urls[this.tipoAnimal];
    this.fotoBonita = true;
    this.radioEsquinas = 500;
  }

  public getClasesFoto() {
    return {
      borde: this.fotoBonita,
      small: !this.fotoBonita
    };
  }
}
